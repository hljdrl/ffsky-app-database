package com.gitee.hljdrl.database;

import android.content.ContentValues;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

public class SettingTable extends AbstractAccountTable {


    public static final class Fields implements AbstractAccountTable.Fields {

        public static final String TAB_NAME = "_SettingTable";
        //
        public static final String DATA_KEY = "_DATA_KEY";
        public static final String DATA_BODY = "_DATA_BODY";
        public static final String DATA_TIME = "_DATA_TIME";

        public static final String COLUMNS[] = {_ID, DATA_KEY, DATA_BODY, DATA_TIME};
    }

    private static SettingTable instance;

    public static SettingTable getInstance() {
        if (instance == null) {
            instance = new SettingTable();
        }
        return instance;
    }

    private SettingTable() {
    }

    @Override
    public void create(SQLiteDatabase db) {
        String sql;
        sql = "CREATE TABLE " + SettingTable.Fields.TAB_NAME + " ("
                + SettingTable.Fields._ID + " INTEGER PRIMARY KEY,"
                + SettingTable.Fields.ACCOUNT + " TEXT,"
                + SettingTable.Fields.DATA_KEY + " TEXT,"
                + SettingTable.Fields.DATA_TIME + " INTEGER,"
                + SettingTable.Fields.DATA_BODY + " TEXT);";
        DataBaseManager.execSQL(db, sql);
    }

    @Override
    protected String getTableName() {
        return SettingTable.Fields.TAB_NAME;
    }

    @Override
    protected String[] getProjection() {
        return SettingTable.Fields.COLUMNS;
    }

    public void removeAccount() {
        removeAccount(AccountCache.getCacheAccount());
    }

    public int hasItem(String key) {
        SQLiteDatabase db = DataBaseManager.getInstance().getReadableDatabase();
        Cursor c = db.query(getTableName(), getProjection(), Fields.ACCOUNT + "=? AND " + SettingTable.Fields.DATA_KEY + "=?", new String[]{AccountCache.getCacheAccount(), key}, null, null, null);
        if (hasData(c)) {
            c.moveToFirst();
            int _id = c.getInt(c.getColumnIndex(SettingTable.Fields._ID));
            closeCursor(c);
            return _id;
        } else {
            closeCursor(c);
            return -1;
        }
    }

    public int deleteItem(String key) {
        try {
            SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
            int _id = db.delete(getTableName(), SettingTable.Fields.ACCOUNT + "=? AND " + SettingTable.Fields.DATA_KEY + "=?", new String[]{AccountCache.getCacheAccount(), key});
            return _id;
        } catch (Exception ex) {
        }
        return 0;
    }

    /**
     * @param key  名字
     * @param body 数值
     * @return 保存id
     */
    public int saveItem(String key, String body) {
        ContentValues cv = new ContentValues();
        cv.put(Fields.ACCOUNT, AccountCache.getCacheAccount());
        cv.put(SettingTable.Fields.DATA_KEY, key);
        cv.put(SettingTable.Fields.DATA_BODY, body);
        cv.put(SettingTable.Fields.DATA_TIME, System.currentTimeMillis());

        SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
        int $id = hasItem(key);
        if ($id > -1) {
            int $rid = db.update(getTableName(), cv, SettingTable.Fields._ID + "=?", new String[]{String.valueOf($id)});
            return $rid;
        } else {
            long id = db.insert(getTableName(), null, cv);
            return (int) id;
        }
    }

    protected String readItem(String name) {
        String _result = null;
        Cursor c = list(Fields.ACCOUNT + "=? AND " + SettingTable.Fields.DATA_KEY + "=?", new String[]{AccountCache.getCacheAccount(), name});
        try {
            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                String readValue = getStringValue(c, SettingTable.Fields.DATA_BODY);
                _result = readValue;
            }
        } finally {
            closeCursor(c);
        }
        return _result;
    }

    public String readItem(String name, String defaultValue) {
        String result = readItem(name);
        if (result == null) {
            return defaultValue;
        }
        return result;
    }

    public String readItemAsString(String name, String _default) {
        String result = readItem(name);
        if (result == null) {
            return _default;
        }
        return result;
    }

    public int readItemAsInt(String name, int _default) {
        String result = readItem(name);
        if (result == null) {
            return _default;
        }
        try {
            return Integer.parseInt(result);
        } catch (Exception ex) {
            //TODO
        }
        return _default;
    }

    public long readItemAsLong(String name, long _default) {
        String result = readItem(name);
        if (result == null) {
            return _default;
        }
        try {
            return Long.parseLong(result);
        } catch (Exception ex) {
            //TODO
        }
        return _default;
    }

    public float readItemAsFloat(String name, float _default) {
        String result = readItem(name);
        if (result == null) {
            return _default;
        }
        try {
            return Float.parseFloat(result);
        } catch (Exception ex) {
            //TODO
        }
        return _default;
    }

    public double readItemAsDouble(String name, double _default) {
        String result = readItem(name);
        if (result == null) {
            return _default;
        }
        try {
            return Double.parseDouble(result);
        } catch (Exception ex) {
            //TODO
        }
        return _default;
    }

    public boolean readItemAsBoolean(String name, boolean _default) {
        String result = readItem(name);
        if (result == null) {
            return _default;
        }
        try {
            return Boolean.parseBoolean(result);
        } catch (Exception ex) {
            //TODO
        }
        return _default;
    }
}
