package com.gitee.hljdrl.database;



import android.util.Log;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

public  abstract class AbstractTable implements DatabaseTable {

	protected abstract String getTableName();

	protected abstract String[] getProjection();
	

	protected String getListOrder() {
		return null;
	}

	@Override
	public void migrate(SQLiteDatabase db, int toVersion) {
		DataBaseManager.dropTable(db, getTableName());
	}
	public  final String getStringValue(Cursor cursor,String ColumnName){
		return cursor.getString(cursor.getColumnIndex(ColumnName));
	}
	public final int getIntValue(Cursor cursor,String ColumnName){
		return cursor.getInt(cursor.getColumnIndex(ColumnName));
	}
	/**
	 * Query table.
	 * 
	 * @return Result set with defined projection and in defined order.
	 */
	public Cursor list() {
		SQLiteDatabase db = DataBaseManager.getInstance().getReadableDatabase();
		return db.query(getTableName(), getProjection(), null, null, null,
				null, getListOrder());
	}
	public int getCount(){
		int count = 0;
		Cursor _cursor = list();
		if(_cursor!=null){
			count = _cursor.getCount();
		}
		closeCursor(_cursor);
		return count;
	}

	protected void addTableColumn(SQLiteDatabase db,String tableName,String columnName,String sql){
		if(!checkColumnExist(db,tableName,columnName)){
			//
//			String sql = "ALTER TABLE " + tableName + " ADD COLUMN " + columnName + " INTEGER(4) NOT NULL default 0";
			db.execSQL(sql);
		}
	}

	/**
	 * 方法1：检查某表列是否存在
	 * @param db
	 * @param tableName 表名
	 * @param columnName 列名
	 * @return
	 */
	protected boolean checkColumnExist(SQLiteDatabase db, String tableName
			, String columnName) {
		boolean result = false ;
		Cursor cursor = null ;
		try{
			//查询一行
			cursor = db.rawQuery( "SELECT * FROM " + tableName + " LIMIT 0"
					, null );
			result = cursor != null && cursor.getColumnIndex(columnName) != -1 ;
		}catch (Exception e){
			Log.e("AbstractTable","checkColumnExists1..." + e.getMessage()) ;
		}finally{
			if(null != cursor && !cursor.isClosed()){
				cursor.close() ;
			}
		}
		return result ;
	}

	/**
	 * 方法2：检查表中某列是否存在
	 * @param db
	 * @param tableName 表名
	 * @param columnName 列名
	 * @return
	 */
	protected boolean checkColumnExistsAsSqliteMaster(SQLiteDatabase db, String tableName
			, String columnName) {
		boolean result = false ;
		Cursor cursor = null ;

		try{
			cursor = db.rawQuery( "select * from sqlite_master where name = ? and sql like ?"
					, new String[]{tableName , "%" + columnName + "%"} );
			result = null != cursor && cursor.moveToFirst() ;
		}catch (Exception e){
			Log.e("AbstractTable","checkColumnExists2..." + e.getMessage()) ;
		}finally{
			if(null != cursor && !cursor.isClosed()){
				cursor.close() ;
			}
		}

		return result ;
	}

	@Override
	public void checkTableColumn(SQLiteDatabase db, String tableName) {

	}

	public Cursor list(String select, String[] selectionArgs) {
		SQLiteDatabase db = DataBaseManager.getInstance().getReadableDatabase();
		return db.query(getTableName(), getProjection(), select, selectionArgs, null,
				null, getListOrder());
	}
	public Cursor list(String select,String[] selectionArgs,String groupBy) {
		SQLiteDatabase db = DataBaseManager.getInstance().getReadableDatabase();
		return db.query(getTableName(), getProjection(), select, selectionArgs, groupBy,
				null, getListOrder());
	}
	public Cursor list(String select,String[] selectionArgs,String groupBy,String order) {
		SQLiteDatabase db = DataBaseManager.getInstance().getReadableDatabase();
		return db.query(getTableName(), getProjection(), select, selectionArgs, groupBy,
				null, order);
	}

	@Override
	public void clear() {
		SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
		db.delete(getTableName(), null, null);
	}
	protected boolean hasData(Cursor c){
		if(c!=null && c.getCount()>0){
			return true;
		}
		return false;
	}
	protected void closeCursor(Cursor c) {
		if (c != null && !c.isClosed()) {
			c.close();
		}
	}

	protected  String string(String... str) {
		StringBuffer buffer = new StringBuffer();
		if (str != null) {
			for (String _s : str) {
				buffer.append(_s);
			}
		}
		return buffer.toString();
	}

	protected  String string(Object... str) {
		StringBuffer buffer = new StringBuffer();
		if (str != null) {
			for (Object _s : str) {
				if (_s != null) {
					buffer.append(_s);
				}
			}
		}
		return buffer.toString();
	}

}
