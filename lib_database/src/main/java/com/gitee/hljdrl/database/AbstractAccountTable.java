package com.gitee.hljdrl.database;

import android.provider.BaseColumns;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

public abstract  class AbstractAccountTable extends AbstractTable{

    public interface Fields extends BaseColumns {

        String ACCOUNT = "account";

    }
    public void removeAccount(String account) {
        SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
        db.delete(getTableName(), Fields.ACCOUNT + " = ?",
                new String[]{account.toString()});
    }
    public static String getAccount(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.ACCOUNT));
    }
}
