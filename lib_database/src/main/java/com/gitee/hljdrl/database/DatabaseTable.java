package com.gitee.hljdrl.database;


import net.sqlcipher.database.SQLiteDatabase;

interface DatabaseTable {

	/**
	 * Called on create database.
	 * 
	 * @param db
	 */
	void create(SQLiteDatabase db);

	/**
	 * Called on database migration.
	 * 
	 * @param db
	 * @param toVersion
	 */
	void migrate(SQLiteDatabase db, int toVersion);

	/**
	 * Called on clear database request.
	 */
	void clear();

	/**
	 * 检查表字段是否缺少
	 * @param db
	 * @param tableName
	 */
	void  checkTableColumn(SQLiteDatabase db,String tableName);
	
	

}
