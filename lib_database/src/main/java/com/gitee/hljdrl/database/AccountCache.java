package com.gitee.hljdrl.database;

public final class AccountCache {

    private static String cacheAccount = "default";

    public static void setCacheAccount(String account) {
        AccountCache.cacheAccount = account;
    }

    public static String getCacheAccount() {
        return cacheAccount;
    }
}
