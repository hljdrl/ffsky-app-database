package com.ffsky.template.demo;

import com.ffsky.template.demo.data.MyLinkSource;
import com.gitee.hljdrl.ffkit.FFKitApplication;
import com.gitee.hljdrl.ffkit.builder.FFKitLinkSource;

public class MyApplication extends FFKitApplication {
    @Override
    public Class<? extends FFKitLinkSource> getLinkSource() {
        return MyLinkSource.class;
    }
}
