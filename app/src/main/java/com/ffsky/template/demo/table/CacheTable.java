package com.ffsky.template.demo.table;

import android.content.ContentValues;
import android.provider.BaseColumns;

import com.gitee.hljdrl.database.AbstractTable;
import com.gitee.hljdrl.database.DataBaseManager;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

public class CacheTable extends AbstractTable {

    public static final class Fields implements BaseColumns {

        public static final String TAB_NAME         = "_CacheTable"  ;
        //
        public static final String DATA_KEY         = "_DATA_KEY"      ;
        public static final String DATA_BODY        = "_DATA_JSON"     ;

        public static final String COLUMNS[] = {_ID,DATA_KEY,DATA_BODY};
    }

    private static CacheTable instance;
    public static CacheTable getInstance() {
        if(instance==null){
            instance = new CacheTable();
        }
        return instance;
    }
    private CacheTable() {
    }

    @Override
    public void create(SQLiteDatabase db) {
        String sql;
        sql = "CREATE TABLE " + CacheTable.Fields.TAB_NAME + " ("
                + CacheTable.Fields._ID+ " INTEGER PRIMARY KEY,"
                + CacheTable.Fields.DATA_KEY + " TEXT,"
                + CacheTable.Fields.DATA_BODY +  " TEXT);";
        DataBaseManager.execSQL(db, sql);

    }


    public int saveItem(String key, String body){
        ContentValues cv = new ContentValues();
        cv.put(CacheTable.Fields.DATA_KEY, key);
        cv.put(CacheTable.Fields.DATA_BODY, body);

        SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
        int $id = hasItem(key);
        if($id>-1){
            int $rid = db.update(getTableName(), cv, CacheTable.Fields._ID+"=?", new String[]{String.valueOf($id)});
            return $rid;
        }else{
            long id = db.insert(getTableName(), null, cv);
            return (int) id;
        }
    }
    public int deleteItem(String key){
        try {
            SQLiteDatabase db = DataBaseManager.getInstance().getWritableDatabase();
            int _id = db.delete(getTableName(), CacheTable.Fields.DATA_KEY + "=?", new String[]{key});
            return _id;
        }catch (Exception ex){
        }
        return 0;
    }

    public int hasItem(String key){
        SQLiteDatabase db = DataBaseManager.getInstance().getReadableDatabase();
        Cursor c = db.query(getTableName(), getProjection(), CacheTable.Fields.DATA_KEY+"=?", new String[]{key}, null, null, null);
        if (hasData(c)) {
            c.moveToFirst();
            int _id = c.getInt(c.getColumnIndex(CacheTable.Fields._ID));
            closeCursor(c);
            return _id;
        } else {
            closeCursor(c);
            return -1;
        }
    }

    public String readItem(String _key,String _default)
    {
        String _result = _default;
        Cursor  c = list(CacheTable.Fields.DATA_KEY+"=?",new String[]{_key});
        try{
            if(c!=null && c.getCount()>0){
                c.moveToFirst();
                String $body = getStringValue(c, CacheTable.Fields.DATA_BODY);
                _result = $body;
            }
        }finally {
            closeCursor(c);
        }
        return _result;
    }

    @Override
    protected String getTableName() {
        return CacheTable.Fields.TAB_NAME;
    }

    @Override
    protected String[] getProjection() {
        return CacheTable.Fields.COLUMNS;
    }
}
