package com.ffsky.template.demo.data;


import android.app.Activity;
import android.graphics.Color;
import android.widget.Toast;

import com.ffsky.template.demo.table.CacheTable;
import com.gitee.hljdrl.database.AccountCache;
import com.gitee.hljdrl.database.DataBaseManager;
import com.gitee.hljdrl.database.KVTable;
import com.gitee.hljdrl.database.SettingTable;
import com.gitee.hljdrl.ffkit.bean.FFKitLink;
import com.gitee.hljdrl.ffkit.builder.FFKitLinkSource;
import com.gitee.hljdrl.ffkit.listener.OnExecuteListener;

import java.util.ArrayList;
import java.util.List;

public class MyLinkSource extends FFKitLinkSource {


    @Override
    public boolean isOverlayList() {
        return true;
    }

    @Override
    public List<FFKitLink> getList() {
        List<FFKitLink> list = new ArrayList<>();
        //----------------------------------------------------------------

        list.add(new FFKitLink.Builder().setName("Database init").setColor(Color.BLACK).setTextColor(Color.WHITE).setDesc("必须先初始数据库").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                DataBaseManager.install(activity.getApplication(), "database.db", 1, null);
                Toast.makeText(activity, "DataBaseManager.install ok", Toast.LENGTH_SHORT).show();
            }
        }).build());

        list.add(new FFKitLink.Builder().setName("KVTable save").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                KVTable.getInstance().saveItem("app_set", System.currentTimeMillis() + "");
                Toast.makeText(activity, " KVTable.getInstance().writeItem ...", Toast.LENGTH_SHORT).show();
            }
        }).build());

        list.add(new FFKitLink.Builder().setName("KVTable read").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                String readItem = KVTable.getInstance().readItem("app_set", null);

                Toast.makeText(activity, " KVTable readItem: key=app_set ,value=" + readItem, Toast.LENGTH_SHORT).show();
            }
        }).build());

        list.add(new FFKitLink.Builder().setName("KVTable clear").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                KVTable.getInstance().clear();
                Toast.makeText(activity, " KVTable clear", Toast.LENGTH_SHORT).show();
            }
        }).build());

        list.add(new FFKitLink.Builder().setName("add CacheTable,save,read").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                DataBaseManager.getInstance().addTable(CacheTable.getInstance());
                DataBaseManager.getInstance().checkTable(CacheTable.getInstance());
                CacheTable.getInstance().saveItem("json", "save-json");
                String var = CacheTable.getInstance().readItem("json", null);
                Toast.makeText(activity, " read: " + var, Toast.LENGTH_SHORT).show();
            }
        }).build());
        //---------------------------------------------------------------------------------

        list.add(new FFKitLink.Builder().setName("SettingTable Set ABC").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                AccountCache.setCacheAccount("abc");
                Toast.makeText(activity, "setCacheAccount=abc", Toast.LENGTH_SHORT).show();
            }
        }).build());

        list.add(new FFKitLink.Builder().setName("SettingTable Set xyz").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                AccountCache.setCacheAccount("xyz");
                Toast.makeText(activity, "setCacheAccount=xyz", Toast.LENGTH_SHORT).show();
            }
        }).build());

        list.add(new FFKitLink.Builder().setName("SettingTable read Account").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                Toast.makeText(activity, "getCacheAccount="+AccountCache.getCacheAccount(), Toast.LENGTH_SHORT).show();
            }
        }).build());


        list.add(new FFKitLink.Builder().setName("SettingTable save").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                int id = SettingTable.getInstance().saveItem("app_set", System.currentTimeMillis() + "");
                Toast.makeText(activity, "saveItem,id=" + id, Toast.LENGTH_SHORT).show();
            }
        }).build());

        list.add(new FFKitLink.Builder().setName("SettingTable read").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                String readItem = SettingTable.getInstance().readItem("app_set", null);

                Toast.makeText(activity, " SettingTable readItem: key=app_set ,value=" + readItem, Toast.LENGTH_SHORT).show();
            }
        }).build());

        list.add(new FFKitLink.Builder().setName("SettingTable clear").setOnExecuteListener(new OnExecuteListener() {
            @Override
            public void onExecute(Activity activity) {
                int count = SettingTable.getInstance().getCount();
                SettingTable.getInstance().clear();
                int new_count = SettingTable.getInstance().getCount();
                Toast.makeText(activity, "SettingTable clear,Count=" + count + " newCount=" + new_count, Toast.LENGTH_SHORT).show();
            }
        }).build());


        DataBaseManager.install(getActivity().getApplication(), "database.db", 1, null);

        return list;
    }


    @Override
    public String getAppName() {
        return "Database app";
    }
}
